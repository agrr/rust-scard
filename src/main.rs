extern crate libc;
use libc::c_void;
use libc::c_long;
use libc::c_ulong;
use std::ptr;


#[repr(C)]
struct Scard_IO_Request
{
proto: u32,
pci_length: u32

}
#[link(name = "pcsclite")]
extern {
    fn SCardEstablishContext(dwScope: u32, pvReserved1 : *const c_void, pvReserved2 : *const c_void, phContext: *mut c_long) -> c_long;
    fn SCardListReaders(phContext: c_long, notUsed: *const c_void, mszReaders: *mut u8, size: *mut u32) -> c_long;
    fn SCardConnect(phContext: c_long, reader_name: * const u8, share_mode: c_long, preferred_protocols: c_long, card: *mut c_long,
     active_protocol: *mut c_long) -> c_long;
    fn SCardTransmit(card: c_long, send_pci: *const Scard_IO_Request, sendBuffer: *const u8, sendlength: usize,
        recv_pci: *mut Scard_IO_Request, recvBuffer: *mut u8, recvLength: *mut c_ulong) -> c_long;
}

	
const SCARD_E_INSUFFICIENT_BUFFER:i64  = 0x80100008;
const SCARD_E_NO_SMARTCARD:i64		   = 0x8010000C;
const SCARD_E_NO_READERS_AVAILABLE:i64 = 0x8010002E;
    
const SCARD_PROTOCOL_T0:i64 = 1;
const SCARD_PROTOCOL_T1:i64 = 2;

fn send_apdu(card_handle: c_long, protocol:c_long, my_apdu: &[u8]) -> bool
{
	let t0_io_request: Scard_IO_Request = Scard_IO_Request { proto: 0x0001, pci_length: 16  }; //sizeof(SCARD_IO_REQUEST): 16???
	let t1_io_request: Scard_IO_Request = Scard_IO_Request { proto: 0x0002, pci_length: 16  };
	 
	let mut recv_buffer = vec![0u8; 300];
	let mut recv_pci : Scard_IO_Request = Scard_IO_Request { proto: 0x0000, pci_length: 0 };
	let mut recv_len:c_ulong = 300;

    let scard_io_req = if protocol == SCARD_PROTOCOL_T0 {t0_io_request} else {t1_io_request};

	unsafe
	{
	 	let ret = SCardTransmit(card_handle, &scard_io_req, &my_apdu[0], my_apdu.len(), &mut recv_pci, recv_buffer.as_mut_ptr(), &mut recv_len);
	 	
		if ret == 0
		{
			
			return recv_buffer[(recv_len-2) as usize] == 0x90 && recv_buffer[(recv_len-1) as usize] == 0;	
		 	
		}
        else if ret == SCARD_E_INSUFFICIENT_BUFFER
        {
			println!("SCardTransmit failed with insufficient buffer *recvLength={}", recv_len);
            return false;

        }
		else {
			println!("SCardTransmit failed! with error= 0x{:X}", ret);
			return false;
		}

	 };

}

//TODO
// fn verify_pin(card_handle: c_long, pin_id: uint, pin_value: str)
// {

// }

	//Specific error codes from pcsclite.h, not sure if this is consistent with Windows error codes...


fn main() {

	let mut context: libc::c_long = 0;
	let mut card_handle: libc::c_long = 0;
	let mut active_protocol: libc::c_long = 0;

	unsafe {

		let ret = SCardEstablishContext(0x0000, ptr::null(), ptr::null(), &mut context); 
		println!("ret= {} context = 0x{:X}", ret, context);

		let mut size = 300;
		let mut readers = vec![0; 300];
		let ret2 = SCardListReaders(context, ptr::null(), readers.as_mut_ptr(),  &mut size);

		if ret2 == SCARD_E_NO_READERS_AVAILABLE {
			println!("No smart card readers available!");
		}
		else {
		
			//println!("Found readers: #1: {}", reader_name);

			//let reader_c_str = reader_name.to_c_str_unchecked();
			let ret_connect = SCardConnect(context, &readers[0], 0x0001, SCARD_PROTOCOL_T0 | SCARD_PROTOCOL_T1, &mut card_handle, &mut active_protocol);

			if ret_connect == SCARD_E_NO_SMARTCARD {
				println!("No smart card available!");
			}
			else if ret_connect == 0 {

				println!("Connected with protocol= 0x{} ", active_protocol);
				//SELECT Gemsafe Application
				let select_app_apdu = [0x00, 0xA4, 0x04, 0x0C, 0x07, 0x60, 0x46, 0x32, 0xFF, 0x00, 0x00, 0x02];
				if send_apdu(card_handle, active_protocol, &select_app_apdu)
				{
					if send_apdu(card_handle, active_protocol, &[0x00, 0xA4, 0x00, 0x0C, 0x02, 0x3F, 0x00])
					{
						println!("Success sending first Select File!");		
					}
					if send_apdu(card_handle, active_protocol, &[0x00, 0xA4, 0x00, 0x0C, 0x02, 0x5F, 0x00])
					{
						println!("Success sending second Select File!");		
					}
				}
				else {
					println!("SELECT application failed!");
				}

			}
			else{
				println!("SCardConnect returned error= 0x{:X} ", ret_connect);
			}

		}


	};

}
